import React, {} from "react";
import natas from "./natas.png"

import './App.css';
import Button from "./components/Button";

function App() {
    return (
        <div className="App">
            <h1><img width="150" src={natas} alt="Natas"/></h1>
            <Button title={"Add Pastel de Nata <br />[pɐʃˈtɛɫ dɯ ˈnatɐ]"}/>
            <br/>
            <p>
                <small>
                    <a href="https://de.wikipedia.org/wiki/Pastel_de_Nata"
                       target={'_blank'}>https://de.wikipedia.org/wiki/Pastel_de_Nata</a>
                </small>
            </p>
        </div>
    );
}

export default App;

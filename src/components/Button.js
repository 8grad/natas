import React, {useState, useEffect} from 'react'
import natas from "../natas.png"

function Button() {
    // Declare a new state variable, which we'll call "count"
    const [count, setCount] = useState(0);

    console.log(count)

    useEffect(() => {
        setCount(JSON.parse(window.localStorage.getItem('count')));
    }, []);

    useEffect(() => {
        window.localStorage.setItem('count', count);
    }, [count]);

    const increaseCount = () => {
        return setCount(count + 1);
    }
    const decreaseCount = () => {
        return setCount(count - 1)
    }

    return (
        <div>
            <p>You ate <b>{count || 0}</b> Pastel de Nata <br /><i>[pɐʃˈtɛɫ dɯ ˈnatɐ]</i>
            </p>

            <button onClick={decreaseCount}>➖</button>
            &nbsp;
            <button onClick={increaseCount}>➕</button>

            <h2>
                {
                    Array(count).fill(null).map((value, index) => (
                        <span><img width="30" src={natas} alt="Natas"/></span>
                    ))
                }
            </h2>

            <pre>
                <table>
                    <tr>
                        <td>carbs:</td>
                        <td>{11.0 * count}g</td>
                    </tr>

                    <tr>
                        <td>fat:</td>
                        <td>{8.0 * count}g</td>
                    </tr>

                    <tr>
                        <td>protein:</td>
                        <td>{2.0 * count}g</td>
                    </tr>

                    <tr>
                        <td>kcal:</td>
                        <td><b>{127.0 * count}</b></td>
                    </tr>


                </table>

            </pre>
        </div>
    );
}

export default Button;

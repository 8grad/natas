import React, {Component, useState, setState, useEffect, useContext, useRef} from "react";

import Button from "./Button";
const count = 0;

export default class Counter extends Component {
    render(){
        let { title, task } = this.props
        return(
            <div>
                <div className="count">
                    <h3>Count:</h3>
                    <h1>{count}</h1>
                </div>

                <Button title={"Add Pastel de Nata [pɐʃˈtɛɫ dɯ ˈnatɐ]"}/>
                <br/>
                <p>
                    <a href="https://de.wikipedia.org/wiki/Pastel_de_Nata"
                       target={'_blank'}>https://de.wikipedia.org/wiki/Pastel_de_Nata</a>
                </p>
            </div>
        )}
}
